package com.devcamp.s10_1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class S101Application {

	public static void main(String[] args) {
		SpringApplication.run(S101Application.class, args);
	}

}
