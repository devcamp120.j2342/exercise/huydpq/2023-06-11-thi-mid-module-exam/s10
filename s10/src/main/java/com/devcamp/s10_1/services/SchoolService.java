package com.devcamp.s10_1.services;

import java.util.ArrayList;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.s10_1.models.School;

@Service
public class SchoolService {
    @Autowired
    private ClassroomService classroomService;

    static School school1 = new School(1, "School 1", "Address 1");
    static School school2 = new School(2, "School 2", "Address 2");
    static School school3 = new School(3, "School 3", "Address 3");

    public ArrayList<School> getAllSchools() {
        ArrayList<School> allSchools = new ArrayList<>();

        school1.setClassrooms(classroomService.getClassroomsShool1());
        school2.setClassrooms(classroomService.getClassroomsShool2());
        school3.setClassrooms(classroomService.getClassroomsShool3());

        allSchools.add(school1);
        allSchools.add(school2);
        allSchools.add(school3);

        return allSchools;
    }

    public static int getTotalStudent() {
        int total = 0;
        for (School school : Arrays.asList(school1, school2, school3)) {
            total += school.getTotalStudent();
        }
        return total;
    }

    public School getSchoolById(int schoolId) {
        ArrayList<School> schools = getAllSchools();

        School findSchool = new School();

        for (School regionSchool : schools) {
            if (regionSchool.getId() == schoolId) {
                findSchool = regionSchool;
            }
        }
        return findSchool;
    }

    public ArrayList<School> getSchoolsWithMoreStudents(int noNumber) {
        ArrayList<School> result = new ArrayList<>();
        for (School school : getAllSchools()) {
            if (school.getTotalStudent() > noNumber) {
                result.add(school);
            }
        }
        return result;
    }
}
